# KubeVaultNinja16

## This is a journey of Blockchain, K8 and Vault integration 

Install Vault and consul via docker:

Clone thi repo

https://github.com/chrisolido/vault-consul-docker.git

docker-compose up -d --build

To interact with both Vault and Consul. View the UIs at
Vault UI: http://localhost:8200/ui
Consul UI: http://localhost:8500/ui

docker-compose exec vault bash

vault operator init

Note: save the result

vault operator unseal
Unseal Key (will be hidden):

Note: Do this and use the 3 keys.

vault login
Token (will be hidden):

Note: use the token.

Login to the UI of Vault and consul.
Vault UI: http://localhost:8200/ui
Consul UI: http://localhost:8500/ui

To create secrets: 

## CLI:

$ vault kv put secret/foo bar=precious

Success! Data written to: secret/foo
Read:

$ vault kv get secret/foo

=== Data ===
Key    Value
---    -----
bar    precious

To work with different versions of a specific key, we'll need to upgrade to v2 of the Key/Value backend:

$ vault kv enable-versioning secret/

Success! Tuned the secrets engine at: secret/
Add version 2 by updating the value to copper:

$ vault kv put secret/foo bar=copper

Key              Value
---              -----
created_time     2018-07-24T19:21:05.3966846Z
deletion_time    n/a
destroyed        false
version          2
Read version 1:

$ vault kv get -version=1 secret/foo

====== Metadata ======
Key              Value
---              -----
created_time     2018-07-24T19:17:17.5578234Z
deletion_time    n/a
destroyed        false
version          1

=== Data ===
Key    Value
---    -----
bar    precious
Read version 2:

$ vault kv get -version=2 secret/foo

====== Metadata ======
Key              Value
---              -----
created_time     2018-07-26T21:56:39.7152485Z
deletion_time    n/a
destroyed        false
version          2

=== Data ===
Key    Value
---    -----
bar    copper
Delete the latest version (e.g., version 2):

$ vault kv delete secret/foo

Success! Data deleted (if it existed) at: secret/foo
Delete version 1:

$ vault kv delete -versions=1 secret/foo

Success! Data deleted (if it existed) at: secret/foo
You can undelete as well:

$ vault kv undelete -versions=1 secret/foo

Success! Data written to: secret/undelete/foo
Delete is akin to a soft delete. If you want to remove the underlying metadata, you'll have to use the destroy command:

$ vault kv destroy -versions=1 secret/foo

Success! Data written to: secret/destroy/foo
Review v1 and v2 to view all the available commands.

## API

$export VAULT_TOKEN=(your_token_goes_here)

Create a new secret called foo with a value of world:

$ curl \
    -H "X-Vault-Token: $VAULT_TOKEN" \
    -H "Content-Type: application/json" \
    -X POST \
    -d '{ "data": { "foo": "world" } }' \
    http://127.0.0.1:8200/v1/secret/data/hello
Read the secret:

$ curl \
    -H "X-Vault-Token: $VAULT_TOKEN" \
    -X GET \
    http://127.0.0.1:8200/v1/secret/data/hello
The JSON response should contain a data key with a value similar to:

"data": {
  "data": {
    "foo": "world"
  },
  "metadata": {
    "created_time": "2018-07-24T20:05:28.503281Z",
    "deletion_time": "",
    "destroyed": false,
    "version": 1
  }
}

## Encryption as a Service

docker-compose exec vault bash

Encryption as a Service
Before we look at dynamic secrets, let's quickly review the Transit backend, which can be used as an "encryption as a service" for:

Encrypting and decrypting data "in-transit" without storing it inside Vault
Easily integrating encryption into your application workflow
Back within the bash session in the container, enable Transit:

$ vault secrets enable transit

Success! Enabled the transit secrets engine at: transit/
Configure a named encryption key:

$ vault write -f transit/keys/foo

Success! Data written to: transit/keys/foo
Encrypt:

$ vault write transit/encrypt/foo plaintext=$(base64 <<< "my precious")

Key           Value
---           -----
ciphertext    vault:v1:/Tun95IT+dVTvDfYiCHdI5rGPSAxgvPcFaDDtneRorQCyBOg
Decrypt:

$ vault write transit/decrypt/foo ciphertext=vault:v1:/Tun95IT+dVTvDfYiCHdI5rGPSAxgvPcFaDDtneRorQCyBOg

Key          Value
---          -----
plaintext    bXkgcHJlY2lvdXMK
Decode:

$ base64 -d <<< "bXkgcHJlY2lvdXMK"

my precious




